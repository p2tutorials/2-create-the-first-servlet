# Let's create a url (or a servlet)

If you're writing an Atlassian add-on, more than likely at some point you'll want to show things to the end user. The simplest way of doing this is through a servlet. So let's create a servlet that says 'hello world!' and then make it pretty.

## Set up the dependencies

In order to create an httpservlet, we'll need to make sure we have the right api available to us. In the pom.xml, underneath the `<dependencies>...</dependencies> tags - add the following maven coordinates:

>         <dependency>
>             <groupId>javax.servlet</groupId>
>             <artifactId>servlet-api</artifactId>
>             <version>2.4</version>
>             <scope>provided</scope>
>         </dependency>

Then make sure that you have `atlas-run` going in a command/terminal window and then open up a second command/terminal window for the same folder and execute `atlas-package`.

`atlas-run` will start Jira for us and install the initial add-on
`atlas-package` will package the add-on and cause Jira to re-install the add-on with the latest version.

## Write the initial code

Go ahead and create a new java class at `com.example.ViewServlet` (src/main/java/com/example/viewServlet ). Make sure the contents is:


```
package com.example;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ViewServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.setContentType("text/plain");
        resp.getWriter().write("Hello world!");
        resp.getWriter().close();
    }
}
```

The above code will set the content-type to be text/plain and write out Hello world! to the web page.

Let's wire this up and give it a url. In atlassian-plugin.xml - add:

```
  <servlet name="View Servlet" key="view-servlet" class="com.example.ViewServlet">
    <url-pattern>/my-view-servlet</url-pattern>
  </servlet>

```

Where:
`name="..."` is a name that the administrator will see in the add-on manager (make it something useful for them - servlet141 is not useful ;) ).

`key="..."` is an internal string to the add-on. It has to be unique (to the add-on) - but again - make it useful.

`class="..."` is the full class name of the Servlet we created above.

`<url-pattern>...</url-pattern>` is the path of where the servlet will be available on the Jira instance. Make sure that this is unique within the Atlassian ecosystem (/config would be a bad path - /example/config would be better). Anything that's put here will have /plugins/servlet prefixed to it.

Once configured, trigger `atlas-package` and the add-on will be reloaded. Load up **http://localhost:2990/jira/plugins/servlet/my-view-servlet** in a browser and you should see Hello World in a brower.

![my-view-servlet.png](my-view-servlet.png)

## Make it pretty

All of Atlassian's products use a library called [SiteMesh](http://wiki.sitemesh.org/wiki/display/sitemesh/Home). It's an awesome thing and really makes things simple for us.

In our case we need to change the content-type to be text/html and change  the html content to:

```
<html>
	<head>
			<meta name="decorator" content="atl.general">
	</head>
	<body>
		Hello World!
	</body>
</html>
```

So in our java code:


```
package com.example;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ViewServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.setContentType("text/html");
        resp.getWriter().write("<html>\n" +
                "\t<head>\n" +
                "\t\t\t<meta name=\"decorator\" content=\"atl.general\">\n" +
                "\t</head>\n" +
                "\t<body>\n" +
                "\t\tHello World!\n" +
                "\t</body>\n" +
                "</html>");
        resp.getWriter().close();
    }
}
```

Execute `atlas-package` and then reload the page in your browser. You should see it in a Jira wrapper:

![sitemesh.png](sitemesh.png)

Change the decorator value to atl.admin or atl.popup to see other potential values. SiteMesh is magic. ;) For more values [https://developer.atlassian.com/search/?q=decorators](https://developer.atlassian.com/search/?q=decorators).

#Next up
Next, we'll make the servlet render dynamic data.



