package com.example;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ViewServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.setContentType("text/html");
        resp.getWriter().write("<html>\n" +
                "\t<head>\n" +
                "\t\t\t<meta name=\"decorator\" content=\"atl.general\">\n" +
                "\t</head>\n" +
                "\t<body>\n" +
                "\t\tHello World!\n" +
                "\t</body>\n" +
                "</html>");
        resp.getWriter().close();
    }
}
